package com.github.bsamartins.planningpoker.user.service.view.repository

import com.github.bsamartins.planningpoker.user.service.view.model.User
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: ReactiveMongoRepository<User, String>