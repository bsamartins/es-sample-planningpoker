package com.github.bsamartins.planningpoker.user.service.view.view

import com.github.bsamartins.planningpoker.user.common.event.UserCreatedEvent
import io.eventuate.DispatchedEvent
import io.eventuate.EventHandlerMethod
import io.eventuate.EventSubscriber
import org.slf4j.LoggerFactory

@EventSubscriber
class UserEventHandler {

    private val log = LoggerFactory.getLogger(UserEventHandler::class.java)

    @EventHandlerMethod
    fun create(event: DispatchedEvent<UserCreatedEvent>) {
        log.info("Got event: {}", event)
    }
}