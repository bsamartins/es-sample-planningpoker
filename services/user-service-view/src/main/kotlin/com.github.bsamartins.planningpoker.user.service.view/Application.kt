package com.github.bsamartins.planningpoker.user.service.view

import io.eventuate.javaclient.spring.common.EventuateCommonConfiguration
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import

@SpringBootApplication
@Import(value = [EventuateCommonConfiguration::class])
open class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}