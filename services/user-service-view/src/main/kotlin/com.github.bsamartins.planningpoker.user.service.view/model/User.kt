package com.github.bsamartins.planningpoker.user.service.view.model

import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "users")
class User {
    var id: String? = null
    var name: String? = null
    var email: String? = null
    var password: String? = null
}