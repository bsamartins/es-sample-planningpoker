package com.github.bsamartins.planningpoker.user.service.view.config

import io.eventuate.javaclient.spring.EnableEventHandlers
import io.eventuate.local.java.jdbckafkastore.EventuateLocalConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import


@Configuration
@EnableEventHandlers
@Import(value=[EventuateLocalConfiguration::class])
open class EventuateConfig
