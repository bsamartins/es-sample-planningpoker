package com.github.bsamartins.planningpoker.user.common.event

class UserCreatedEvent(
        val id: String,
        val name: String,
        val email: String,
        val password: String): UserEvent()