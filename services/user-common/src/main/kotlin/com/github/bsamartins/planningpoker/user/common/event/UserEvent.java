package com.github.bsamartins.planningpoker.user.common.event;

import io.eventuate.Event;
import io.eventuate.EventEntity;

@EventEntity(entity = "com.github.bsamartins.planningpoker.user.service.UserAggregate")
public class UserEvent implements Event {
}