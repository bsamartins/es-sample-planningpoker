package com.github.bsamartins.planningpoker.user.service;

import com.github.bsamartins.planningpoker.user.common.event.UserCreatedEvent
import com.github.bsamartins.planningpoker.user.service.command.CreateUserCommand
import com.github.bsamartins.planningpoker.user.service.command.UserCommand
import io.eventuate.Event
import io.eventuate.EventUtil
import io.eventuate.ReflectiveMutableCommandProcessingAggregate
import io.eventuate.EventUtil.events
import org.slf4j.LoggerFactory


class UserAggregate: ReflectiveMutableCommandProcessingAggregate<UserAggregate, UserCommand>() {

    private val log = LoggerFactory.getLogger(UserAggregate::class.java)

    var id: String? = null
    var name: String? = null
    var email: String? = null
    var password: String? = null

    fun process(cmd: CreateUserCommand): List<Event> {
        log.info("Calling BoardAggregate.process for CreateBoardCommand: {}", cmd)
        return EventUtil.events(UserCreatedEvent("", cmd.name, cmd.email, cmd.password))
    }

    fun apply(event: UserCreatedEvent) {
        log.info("Got event: {}", event)
        this.id = event.id
        this.name = event.name
        this.email = event.email
        this.password = event.password
    }

}
