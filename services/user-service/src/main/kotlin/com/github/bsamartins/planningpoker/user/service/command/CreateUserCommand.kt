package com.github.bsamartins.planningpoker.user.service.command

class CreateUserCommand(
        val email: String,
        val password: String,
        val name: String): UserCommand