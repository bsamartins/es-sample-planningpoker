package com.github.bsamartins.planningpoker.user.service

import com.github.bsamartins.planningpoker.user.service.command.CreateUserCommand
import com.github.bsamartins.planningpoker.user.service.command.UserCommand
import com.github.bsamartins.planningpoker.user.service.model.UserCreate
import io.eventuate.AggregateRepository
import io.eventuate.EntityWithIdAndVersion
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture

@Service
class UserService(private val aggregateRepository: AggregateRepository<UserAggregate, UserCommand>) {

    fun create(user: UserCreate): CompletableFuture<EntityWithIdAndVersion<UserAggregate>> {
        return this.aggregateRepository.save(CreateUserCommand(user.email, user.password, user.name))
    }

}