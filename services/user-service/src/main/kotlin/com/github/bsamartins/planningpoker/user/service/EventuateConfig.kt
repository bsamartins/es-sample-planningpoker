package com.github.bsamartins.planningpoker.user.service

import com.github.bsamartins.planningpoker.user.service.command.UserCommand
import io.eventuate.AggregateRepository
import io.eventuate.EventuateAggregateStore
import io.eventuate.javaclient.driver.EventuateDriverConfiguration
import io.eventuate.javaclient.spring.EnableEventHandlers
import io.eventuate.local.cdc.debezium.EventTableChangesToAggregateTopicRelayConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import


@Configuration
@EnableEventHandlers
@Import(value=[
    EventuateDriverConfiguration::class,
    EventTableChangesToAggregateTopicRelayConfiguration::class
])
open class EventuateConfig {
    @Bean
    open fun userAggregateRepository(eventStore: EventuateAggregateStore): AggregateRepository<UserAggregate, UserCommand> {
        return AggregateRepository(UserAggregate::class.java, eventStore)
    }
}

