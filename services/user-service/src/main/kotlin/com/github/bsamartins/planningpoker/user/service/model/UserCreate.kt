package com.github.bsamartins.planningpoker.user.service.model

class UserCreate(val email: String, val password: String, val name: String)