package com.github.bsamartins.planningpoker.user.service

import com.github.bsamartins.planningpoker.user.service.model.UserCreate
import io.eventuate.EntityWithIdAndVersion
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.CompletableFuture

@RestController
@RequestMapping("/api/v1/users")
class UserController(val userService: UserService) {

    @PostMapping
    fun create(@Validated @RequestBody userCreate: UserCreate): CompletableFuture<EntityWithIdAndVersion<UserAggregate>> {
        return this.userService.create(userCreate)
    }

}